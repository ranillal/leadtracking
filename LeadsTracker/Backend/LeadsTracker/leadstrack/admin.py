from django.contrib import admin

# Register your models here.
from .models import Leads,Executive,LeadTrack

admin.site.register(Leads)
admin.site.register(Executive)
admin.site.register(LeadTrack)