from django.apps import AppConfig


class LeadstrackConfig(AppConfig):
    name = 'leadstrack'
