from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Leads(models.Model):
    id = models.AutoField(primary_key=True)
    Name = models.TextField(max_length=100)
    Class = models.TextField(max_length=100)
    Phone = models.TextField(max_length=100)
    Email = models.EmailField()
    Address = models.TextField(max_length=200)
    Remarks = models.TextField(max_length=200)
    Status = models.TextField(max_length=100)
    Source = models.TextField(max_length=100)

    def __str__(self):
        return self.Name

class Executive(models.Model):
    id = models.AutoField(primary_key=True)
    Name = models.TextField(max_length=100)
    Phone = models.TextField(max_length=100)
    Email = models.EmailField()
    Status = models.TextField(max_length=100)
    Location =models.TextField(max_length=100)
    
    def __str__(self):
        return self.Name

class LeadTrack(models.Model):
    Lead = models.ForeignKey(to=Leads,on_delete=models.CASCADE,null=True)
    Eecutive = models.ForeignKey(to=Executive, on_delete=models.CASCADE,null=True)
    TrackingCode = models.TextField(max_length=50)
    Date = models.DateTimeField(auto_now=False, auto_now_add=False)
    Contact = models.TextField(max_length=100)
    Status = models.TextField(max_length=100)
    Remarks = models.TextField(max_length=200)
    NextAction =models.TextField(max_length=200)
    
    def __str__(self):
        return self.TrackingCode

